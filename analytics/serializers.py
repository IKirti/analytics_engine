from django.db import transaction
from django.db.models import Avg, Count, Sum, IntegerField
import datetime

from django.db.models import Case
from django.db.models import Value
from django.db.models import When
from rest_framework import (
    serializers as rest_serializer,
    exceptions as rest_exceptions,
    fields as rest_fields,
)
from analytics import models as analytics_models
from commons import utils as commons_utils


class TrackWebPageInfoSerializer(rest_serializer.ModelSerializer):
    page_name = rest_fields.CharField(max_length=255, write_only=True)

    class Meta:
        model = analytics_models.UserTraffic
        fields = (u'session_id', u'duration_per_page', u'page_name', u'latitude', u'longitude',)

    def __init__(self, *args, **kwargs):
        super(TrackWebPageInfoSerializer, self).__init__(*args, **kwargs)
        self.client_app = kwargs[u'context'][u'client_app'] if kwargs.get(u'context') else None

    def validate_page_name(self, attr):
        try:
            page = self.client_app.page_set.get(name=attr)
        except analytics_models.Page.DoesNotExist:
            raise rest_exceptions.ValidationError(detail=u'Page Name is not valid')
        else:
            return page

    @transaction.atomic()
    def create(self, validated_data):
        page = validated_data.pop('page_name')  # many to many field will be stored after creating the instance
        instance = analytics_models.UserTraffic.objects.create(**validated_data)

        # Add app and page info
        instance.page.add(page.id)
        instance.app.add(self.client_app.id)
        return instance


class AverageDurationSerializer(rest_serializer.ModelSerializer):
    average_duration_per_page = rest_serializer.SerializerMethodField()

    class Meta:
        model = analytics_models.ClientApp
        fields = (u'average_duration_per_page',)

    def get_average_duration_per_page(self, instance):
        return analytics_models.UserTraffic.objects.filter(app__id=instance.id).values(
            'page__name').annotate(
            average_duration_per_page=Avg('duration_per_page'))


class UniqueUserCountSerializer(rest_serializer.ModelSerializer):
    """
    Serializer to get number of unique users on a day or week or in a month
    """
    user_count = rest_serializer.SerializerMethodField()

    class Meta:
        model = analytics_models.ClientApp
        fields = (u'user_count',)

    def get_user_count(self, instance):
        # We can take date, week or month as input from user as well to be more flexible
        start_date = datetime.date.today()
        end_date = start_date + datetime.timedelta(days=1)
        start_week = start_date - datetime.timedelta(start_date.weekday())  # current week
        end_week = start_week + datetime.timedelta(7)
        start_month = datetime.date(year=start_date.year, month=start_date.month, day=1)
        day_count = Sum(
            Case(
                When(created_at__range=[start_date, end_date], then=1)
            ),
            default=Value(0),
            output_field=IntegerField()
        )
        week_count = Sum(
            Case(
                When(created_at__range=[start_week, end_week], then=1)
            ),
            default=Value(0),
            output_field=IntegerField()
        )
        month_count = Sum(
            Case(
                When(created_at__gte=start_month, then=1)
            ),
            default=Value(0),
            output_field=IntegerField()
        )

        return instance.related_app_users.all().values(
            'session_id').annotate(
                day_count=day_count, week_count=week_count, month_count=month_count)


class LocationAndPageWiseCountSerializer(rest_serializer.ModelSerializer):
    """
    Serializer to get number of unique users group by location and page name on a day, in a month
    """
    location_and_page_wise_count = rest_serializer.SerializerMethodField()

    class Meta:
        model = analytics_models.ClientApp
        fields = (u'location_and_page_wise_count',)

    def get_location_and_page_wise_count(self, instance):
        start_date = datetime.date.today()
        end_date = start_date + datetime.timedelta(days=1)
        start_month = datetime.date(year=start_date.year, month=start_date.month, day=1)

        day_count = Sum(
            Case(
                When(created_at__range=[start_date, end_date], then=1)
            ),
            default=Value(0),
            output_field=IntegerField()
        )
        month_count = Sum(
            Case(
                When(created_at__gte=start_month, then=1)
            ),
            default=Value(0),
            output_field=IntegerField()
        )

        return instance.related_app_users.all().values(
            'page__name', 'latitude', 'longitude').annotate(
                day_count=day_count, month_count=month_count)


class RealTimeSessionCountSerializer(rest_serializer.ModelSerializer):
    """
    Serializer to get real time number of session
    """
    real_time_session_ids = rest_serializer.SerializerMethodField()

    class Meta:
        model = analytics_models.ClientApp
        fields = (u'real_time_session_ids',)

    def get_real_time_session_ids(self, instance):
        # assuming a user is active if they visit to site 5 minutes ago (Following the
        # standard of Google analytics Engine)
        five_minutes_ago = commons_utils.get_current_datetime() - datetime.timedelta(seconds=5 * 60)

        return instance.related_app_users.filter(created_at__gte=five_minutes_ago).values(
            'session_id').annotate(active_session_count=Count('session_id'))
