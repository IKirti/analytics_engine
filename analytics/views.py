from rest_framework import (
    permissions as rest_permissions,
    exceptions as rest_exceptions,
    generics as rest_generics,
)
from analytics import (
    models as analytics_models,
    serializers as analytics_serializers
)


class BaseApiView(rest_generics.RetrieveAPIView):
    allowed_methods = ['get']
    permission_classes = (rest_permissions.AllowAny,)
    model = analytics_models.ClientApp
    lookup_field = 'token'
    queryset = analytics_models.ClientApp.objects.all()


class TrackWebPageInfoAPIView(rest_generics.CreateAPIView):
    """
    API to capture details from the webpages
    """
    serializer_class = analytics_serializers.TrackWebPageInfoSerializer
    allowed_methods = ['post']
    permission_classes = (rest_permissions.AllowAny,)  # We can use TokenAuthentication here
    client_app = None

    def get_serializer_context(self):
        return dict(
            client_app=self.client_app,
            **super(TrackWebPageInfoAPIView, self).get_serializer_context())

    def get_client_app(self, token):
        try:
            self.client_app = analytics_models.ClientApp.objects.get(token=token)
        except analytics_models.ClientApp.DoesNotExist:
            raise rest_exceptions.ValidationError({u'non_field_errors': u'Email Already verified'})

    def post(self, request, *args, **kwargs):
        self.get_client_app(self.kwargs[u'token'])
        return super(TrackWebPageInfoAPIView, self).post(request, *args, **kwargs)


class AverageDurationAPIView(BaseApiView):
    """
    API to get average duration per page
    """
    serializer_class = analytics_serializers.AverageDurationSerializer


class DayWeekMonthUserCountAPIView(BaseApiView):
    """
    API to get number of unique users on a day or week or in a month
    """
    serializer_class = analytics_serializers.UniqueUserCountSerializer


class RealTimeSessionCountAPIView(BaseApiView):
    """
    Api to get real time number of session
    """
    serializer_class = analytics_serializers.RealTimeSessionCountSerializer


class LocationAndPageWiseCountAPIView(BaseApiView):
    """
    Api to get number of unique users group by location and page name on a day, in a month
    """
    serializer_class = analytics_serializers.LocationAndPageWiseCountSerializer

