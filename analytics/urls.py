from django.conf.urls import url

from analytics import views as analytics_views

urlpatterns = [
    url(r'^track-info/(?P<token>[\-_a-zA-Z0-9]+)/$',
        analytics_views.TrackWebPageInfoAPIView.as_view(), name="track_info"),
    url(r'^average-duration/(?P<token>[\-_a-zA-Z0-9]+)/$',
        analytics_views.AverageDurationAPIView.as_view(), name="get_average_duration"),
    url(r'^day-week-month/count/(?P<token>[\-_a-zA-Z0-9]+)/$',
        analytics_views.DayWeekMonthUserCountAPIView.as_view(), name="get_day_wise_user_count"),
    url(r'^location-page-wise/count/(?P<token>[\-_a-zA-Z0-9]+)/$',
        analytics_views.LocationAndPageWiseCountAPIView.as_view(), name="get_location_and_page_wise_user_count"),
    url(r'^realtime-session/count/(?P<token>[\-_a-zA-Z0-9]+)/$',
        analytics_views.RealTimeSessionCountAPIView.as_view(), name="get_realtime_session_count"),
]
