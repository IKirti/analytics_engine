from django.contrib import admin

from analytics import models

admin.site.register(models.ClientApp)
admin.site.register(models.Page)
admin.site.register(models.UserTraffic)
