# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ClientApp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated At')),
                ('name', models.CharField(max_length=255)),
                ('token', models.CharField(help_text="Unique token of client's app", unique=True, max_length=50, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated At')),
                ('name', models.CharField(max_length=255)),
                ('app', models.ForeignKey(to='analytics.ClientApp')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserTraffic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated At')),
                ('latitude', models.DecimalField(null=True, max_digits=10, decimal_places=6, blank=True)),
                ('longitude', models.DecimalField(null=True, max_digits=10, decimal_places=6, blank=True)),
                ('session_id', models.CharField(max_length=255)),
                ('duration_per_page', models.IntegerField()),
                ('app', models.ManyToManyField(related_name='related_app_users', to='analytics.ClientApp')),
                ('page', models.ManyToManyField(related_name='related_page_users', to='analytics.Page')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
