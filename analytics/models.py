import hashlib

from django.db import models
from commons import (
    models as commons_models,
    utils as commons_utils
)


class ClientApp(commons_models.DatesModel, models.Model):
    # Created at and Updated at are inherit from DatesModel
    name = models.CharField(max_length=255)
    token = models.CharField(max_length=50, unique=True, help_text=u"Unique token of client's app", blank=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = self.generate_token()
        return super(ClientApp, self).save(*args, **kwargs)

    def generate_token(self):
        return hashlib.md5(
            str(self.name) + str(commons_utils.get_current_datetime())
        ).hexdigest()


class Page(commons_models.DatesModel, models.Model):
    name = models.CharField(max_length=255)
    app = models.ForeignKey(ClientApp)  # An app can have multiple pages associated to it

    def __unicode__(self):
        return u"{} is related to app {}".format(self.name, self.app)


class UserTraffic(commons_models.DatesModel, commons_models.Location, models.Model):
    session_id = models.CharField(max_length=255)
    app = models.ManyToManyField(ClientApp, related_name='related_app_users')
    page = models.ManyToManyField(Page, related_name='related_page_users')
    duration_per_page = models.IntegerField()  # in seconds

    def __unicode__(self):
        return u"{} - {} - {}".format(self.app, self.page, self.session_id)
