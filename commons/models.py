import datetime
from django.db import models
from django.utils.translation import ugettext_lazy as _


class DatesModel(models.Model):
    """
    Abstract model for dates
    """
    created_at = models.DateTimeField(_('Created at'), default=datetime.datetime.now)
    updated_at = models.DateTimeField(_('Updated At'), auto_now=True)

    class Meta:
        abstract = True


class Location(models.Model):
    latitude = models.DecimalField(max_digits=10, decimal_places=6, null=True, blank=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=6, null=True, blank=True)

    class Meta:
        abstract = True
